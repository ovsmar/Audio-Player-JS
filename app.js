// fetch
fetch("data.json")
  .then((response) => response.json())
  .then((data) => {
    console.log(data);

    var lastSong = null;
    var selection = null;
    var playlist = data; // List of songs (format json)
    var player = document.getElementById("audioplayer"); // Get audio element
    player.volume = 0.1; // value of volume
    // player.autoplay = false; // off autoplay
    player.autoplay = false;

    player.addEventListener("ended", selectRandom); // Run function when the song ends

    function selectRandom() {
      while (selection == lastSong) {
        // Repeat until a different song is selected
        selection = Math.floor(Math.random() * playlist.length);
      }
      lastSong = selection; // Remember the last song
      player.src = playlist[selection]; // Tell HTML the location of the new song
    }

    selectRandom(); // Select initial song
    player.play(); // Start song
  });
